# PIC Assignment 2 README file

## Description

The purpose of this qt grade calculator is for the user to enter values.
The calculator then responds to this through its widgets interacting with the user.
It then computes an overall score for PIC 10B and PIC 10C classes.
In PIC 10B there are two grading schemes, with the lowest homework dropped and in PIC 10C the lowest homework score is not dropped and there is only one method of computing the overall score.

## Sources consulted

Programming Knowledge, “How to Create First Qt GUI application in C++ using Qt Creator.” YouTube, Uploaded by Programming Knowledge, 24 January 2016. https://www.youtube.com/watch?v=3SIj6zL6mmA&t=546s

Programming Knowledge, “QT C++ GUI Tutorial 20- Database values in QlineEdit or textbox if select Combobox” YouTube, Uploaded by Programming Knowledge, 1 November 2013. 
<https://www.youtube.com/watch?v=BwXqJD-n2O0>

Programming Knowledge, “Qt Tutorials For Beginners 6 – QmessageBox”
 YouTube, Uploaded by Programming Knowledge, 14 April, 2016.
<https://www.youtube.com/watch?v=xJdxE_7IBsU>

 “Spin Boxes Example.” Qt Documentation, doc.qt.io/qt-5/qtwidgets-widgets-spinboxes-example.html. <https://doc.qt.io/qt-5/qtwidgets-widgets-spinboxes-example.html>

“QSpinBox Class.” Qt Documentation, doc.qt.io/qt-5/qspinbox.html.< https://doc.qt.io/qt-5/qspinbox.html>

“QVector Class.” Qt Documentation, doc.qt.io/qt-5/qvector.html#push_back.< https://doc.qt.io/qt-5/qvector.html#push_back>


 
