#include <QVector>
#include "formulas.h"

//computation for scheme A
double final_score_A(int hw_1,int hw_2, int hw_3, int hw_4, int hw_5, int hw_6, int hw_7, int hw_8, int mt_1, int mt_2, int final){
    QVector<int>vector_of_numbers = {hw_1, hw_2, hw_3, hw_4, hw_5, hw_6, hw_7,hw_8};
    for(int i = 0; i <7 ; i++){
        if(vector_of_numbers[i+1]<vector_of_numbers[i]){
            int temp = vector_of_numbers[i];
            vector_of_numbers[i] = vector_of_numbers[i+1];
            vector_of_numbers[i+1] = temp;
        }
    }
   vector_of_numbers.removeFirst(); //removes the first element (lowest score)
   double sum = vector_of_numbers[0];
   for(int i = 0; i < 7; i++){
       sum = vector_of_numbers[i] + sum; //adding seven remnant homework scores together
   }
   double hw_component = (sum/7)*0.25;
   double total = hw_component + (mt_1*0.2) + (mt_2*0.2) + (final*0.35);
  return total;

}


//computation for scheme B

double final_score_B(int hw_1,int hw_2, int hw_3, int hw_4, int hw_5, int hw_6, int hw_7, int hw_8, int mt_1, int mt_2, int final){
    QVector<int>vector_of_numbers = {hw_1, hw_2, hw_3, hw_4, hw_5, hw_6, hw_7,hw_8};
    for(int i = 0; i < 7; i++){ //finding smallest hw_score and putting it at the beginning
        if(vector_of_numbers[i+1]<vector_of_numbers[i]){
            int temp = vector_of_numbers[i];
            vector_of_numbers[i] = vector_of_numbers[i+1];
            vector_of_numbers[i+1] = temp;
        }
    }
   vector_of_numbers.removeFirst(); //removes the first element (lowest score)
   double sum = vector_of_numbers[0];
   for(int i = 0; i < 7; i++){ //summing remaining homeworks
       sum = vector_of_numbers[i] + sum;
   }
   double hw_component = (sum/7)*0.25;
   int highest_midterm = 0;
   if(mt_1 > mt_2){
       highest_midterm = mt_1;
   }
   else {
       highest_midterm = mt_2;
   }
   double total = hw_component + (highest_midterm*0.30) + (final*0.44);
  return total;

}

//assuming the PIC 10C score does not drop lowest homework grade

double PIC_10C_score (int hw_1, int hw_2, int hw_3, int midterm, int final, int final_project){
    double hw_score_average = 0;
    hw_score_average = (hw_1 + hw_2 + hw_3)/3;
    double overall_score = 0;
    overall_score = hw_score_average*0.15 + midterm*0.25 + final*0.30 + final_project*0.30;
    return overall_score;

}
