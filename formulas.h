#pragma once
#ifndef FORMULAS_H
#define FORMULAS_H
#include <QVector>

/* Final working version
 */

//computation for scheme A
double final_score_A(int hw_1,int hw_2, int hw_3, int hw_4, int hw_5, int hw_6, int hw_7, int hw_8, int mt_1, int mt_2, int final);


//computation for scheme B

double final_score_B(int hw_1,int hw_2, int hw_3, int hw_4, int hw_5, int hw_6, int hw_7, int hw_8, int mt_1, int mt_2, int final);

//computation for PIC 10C class overall score
double PIC_10C_score (int hw_1, int hw_2, int hw_3, int midterm, int final, int final_project);
#endif // FORMULAS_H
