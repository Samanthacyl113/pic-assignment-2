#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVector>
#include <QString>
#include "formulas.h"
#include <QPushButton>
#include <QVBoxLayout>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //initialization of private member variables for PIC 10B
    hw_1 = 0; hw_2 = 0; hw_3 = 0; hw_4 = 0; hw_5 = 0; hw_6 = 0; hw_7 = 0; hw_8 = 0;mt_1 = 0;mt_2 = 0;final = 0;

    //initialization of private member variables for PIC 10C

      hw1_C = 0; hw2_C = 0; hw3_C = 0; midterm_C = 0; final_C = 0; final_project = 0;


}


MainWindow::~MainWindow()
{
    delete ui;
}




//the following pieces of code are to connect the value entered from the user and store them into private member variables

void MainWindow::on_spinbox_test_valueChanged(int)
{
    connect(ui->spinbox_test,QOverload<int>::of(&QSpinBox::valueChanged),[=](int i){
        hw_1 = i;
    }
); //the structure and syntax for implementing these types of code are extracted from the Qdocumentation file for QspinBox and also from discussion

}


void MainWindow::on_hw_2_spinbox_valueChanged(int)
{
    connect(ui->hw_2_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int j){
        hw_2 = j;
    }
    );

}



void MainWindow::on_hw_3_spinbox_valueChanged(int)
{
    connect(ui->hw_3_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        hw_3 = k;
    }
    );

}


void MainWindow::on_hw_4_spinbox_valueChanged(int)
{
    connect(ui->hw_4_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        hw_4 = k;
    }
    );


}

void MainWindow::on_hw_5_spinbox_valueChanged(int)
{
    connect(ui->hw_5_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        hw_5 = k;
    }
    );

}



void MainWindow::on_hw_6_spinbox_valueChanged(int)
{
    connect(ui->hw_6_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        hw_6 = k;
    }
    );

}


void MainWindow::on_hw_7_spinbox_valueChanged(int)
{
    connect(ui->hw_7_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        hw_7 = k;
    }
    );


}



void MainWindow::on_hw_8_spinbox_valueChanged(int)
{
    connect(ui->hw_8_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        hw_8 = k;
    }
    );
}


void MainWindow::on_midterm_1_spinbox_valueChanged(int)
{
    connect(ui->midterm_1_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        mt_1 = k;
    }
    );

}

void MainWindow::on_midterm_2_spinbox_valueChanged(int)
{
    connect(ui->midterm_2_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        mt_2 = k;
    }
    );


}

void MainWindow::on_final_spinbox_valueChanged(int)
{
    connect(ui->final_spinbox,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
        final = k;
    }
    );
    std::cout << final;
}


void MainWindow::on_scheme_A_button_clicked()
{
    double final_score = 0;
    final_score = final_score_A(hw_1,hw_2, hw_3, hw_4, hw_5, hw_6, hw_7, hw_8, mt_1, mt_2, final);
    ui->overal_score_label->setText(QString::fromStdString(std::to_string(final_score)+"%"));
}


void MainWindow::on_Scheme_B_button_clicked()
{
    double final_score = 0;
    final_score = final_score_B(hw_1,hw_2, hw_3, hw_4, hw_5, hw_6, hw_7, hw_8, mt_1, mt_2, final);
    ui->overal_score_label->setText(QString::fromStdString(std::to_string(final_score)+"%"));
}


//the below definitions are applicable for the PIC 10C widgets



void MainWindow::on_hw1_c_valueChanged(int)
{
    connect(ui->hw1_c,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
       hw1_C  = k;
    }
    );

}



void MainWindow::on_hw2_C_valueChanged(int)
{
    connect(ui->hw2_C,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
       hw2_C  = k;
    }
    );
}

void MainWindow::on_hw3C_valueChanged(int)
{
    connect(ui->hw3C,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
       hw3_C  = k;
    }
    );

}

void MainWindow::on_midterm_C_valueChanged(int)
{
    connect(ui->midterm_C,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
       midterm_C  = k;
    }
    );

}

void MainWindow::on_final_C_valueChanged(int)
{
    connect(ui->final_C,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
       final_C  = k;
    }
    );
}

void MainWindow::on_final_project_C_valueChanged(int)
{
    connect(ui->final_project_C,QOverload<int>::of(&QSpinBox::valueChanged),[=](int k){
       final_project  = k;
    }
    );

}

//the clicked () slot sends the signal of the overall score to the text label for PIC 10C class

void MainWindow::on_push_button_C_clicked()
{
    double pic_10c_score = 0;
    pic_10c_score = PIC_10C_score(hw1_C,hw2_C, hw3_C, midterm_C, final_C , final_project);
    ui->text_overall_C->setText(QString::fromStdString(std::to_string(pic_10c_score)+"%")); //learnt how to convert numbers to Qstring from discussion notes

}
