#pragma once
#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QLabel>
#include <QSpinBox>
#include <QString>
#include "formulas.h"
#include <QWidget>
#include <QObject>
#include <QPushButton>
#include <QVBoxLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:

public slots:

private slots:
    //represents the slots for PIC 10B Class
    void on_spinbox_test_valueChanged(int); //spinbox 1- for homework 1

    void on_hw_2_spinbox_valueChanged(int);

    void on_hw_3_spinbox_valueChanged(int);

    void on_hw_4_spinbox_valueChanged(int);

    void on_hw_5_spinbox_valueChanged(int);

    void on_hw_6_spinbox_valueChanged(int);

    void on_hw_7_spinbox_valueChanged(int);

    void on_hw_8_spinbox_valueChanged(int);

    void on_midterm_1_spinbox_valueChanged(int);

    void on_midterm_2_spinbox_valueChanged(int);

    void on_final_spinbox_valueChanged(int);

    void on_scheme_A_button_clicked();

    void on_Scheme_B_button_clicked();


//below represents the function declarations for PIC 10C class
    void on_hw1_c_valueChanged(int);

    void on_hw2_C_valueChanged(int);

    void on_hw3C_valueChanged(int);

    void on_midterm_C_valueChanged(int);

    void on_final_C_valueChanged(int);

    void on_final_project_C_valueChanged(int);

    void on_push_button_C_clicked();

private:
    Ui::MainWindow *ui;

    int hw_1,hw_2,hw_3, hw_4,hw_5,hw_6,hw_7,hw_8,mt_1,mt_2,final; //user entered values for PIC 10B

    //below the private member variables represents the user defined values for PIC 10C

    int hw1_C, hw2_C, hw3_C, midterm_C, final_C, final_project;



};
#endif // MAINWINDOW_H
